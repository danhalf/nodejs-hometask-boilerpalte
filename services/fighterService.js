const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

    getFighters() {
        try {
            return FighterRepository.getAll();
        } catch (error) {
            throw Error(error);
        }
    }

    searchFighter(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    create(fighter) {
        const isUserCreate = FighterRepository.create(fighter);
        if (!isUserCreate) {
            throw Error('Fighter was not created');
        }
        return fighter;
    }

    changeFighterInfo(id, user) {
        const searchedFighter = this.searchFighter({id})
        const isFighterUpdate = searchedFighter && FighterRepository.update(id, user);
        if (!isFighterUpdate) {
            throw Error('User was not updated');
        }
        return user;
    }

    deleteFighter(id) {
        const searchedFighter = this.searchFighter({id})
        const isFighterUpdate = searchedFighter && FighterRepository.delete(id);
        if (!isFighterUpdate) {
            throw Error('fighter not deleted');
        }
        return id;
    }
}

module.exports = new FighterService();