const { UserRepository } = require('../repositories/userRepository');

class UserService {

  create(user) {
    const isUserCreate = UserRepository.create(user);
    if (!isUserCreate) {
      throw Error('User was not created');
    }
    return user;
  }

  changeUserInfo(id, user) {
    const searchedUser = this.search({id})
    const isUserUpdate = searchedUser && UserRepository.update(id, user);
    if (!isUserUpdate) {
      throw Error('User was not updated');
    }
    return user;
  }

  deleteUser(id) {
    const searchedUser = this.search({id})
    const isUserDeleted = searchedUser && UserRepository.delete(id);
    if (!isUserDeleted) {
      throw Error('User not deleted');
    }
    return id;
  }

  getUsers() {
    try {
      return UserRepository.getAll();
    } catch (error) {
      throw Error(error);
    }
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();
