const { Router } = require('express');
const UserService = require('../services/userService');
const {
  createUserValid,
  updateUserValid,
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const router = Router();

// TODO: Implement route controllers for user

router.get(
  '/',
  (req, res, next) => {
    try {
      if (!res.err) res.send(UserService.getUsers(res.body));
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.get(
  '/:id',
  (req, res, next) => {
    const { id } = req.params;
    try {
      if (!res.err) res.send(UserService.search({ id }));
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.post(
  '/',
  createUserValid,
  (req, res, next) => {
    try {
      if (!res.err) res.data = UserService.create(req.body);
    } catch (err) {
      res.err = err;
    } finally {
      next();
      res.redirect('/api/users');
    }
  },
  responseMiddleware
);

router.put(
  '/:id',
  updateUserValid,
  (req, res, next) => {
    try {
        const { id } = req.params;
      if (!res.err) res.data = UserService.changeUserInfo(id, req.body);
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
  },
  responseMiddleware
);

router.delete(
    '/:id',
    (req, res, next) => {
        try {
            const { id } = req.params;
            if (!res.err) res.data = UserService.deleteUser(id);
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

module.exports = router;
