const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get(
    '/',
    (req, res, next) => {
        try {
            if (!res.err) res.send(FighterService.getFighters(res.body));
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

router.get(
    '/:id',
    (req, res, next) => {
        const { id } = req.params;
        try {
            if (!res.err) res.send(FighterService.searchFighter({ id }));
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

router.post(
    '/',
    createFighterValid,
    (req, res, next) => {
        try {
            if (!res.err) res.data = FighterService.create(req.body);
        } catch (err) {
            res.err = err;
        } finally {
            next();
            res.redirect('/api/fighters');
        }
    },
    responseMiddleware
);

router.put(
    '/:id',
    updateFighterValid,
    (req, res, next) => {
        try {
            const { id } = req.params;
            if (!res.err) res.data = FighterService.changeFighterInfo(id, req.body);
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

router.delete(
    '/:id',
    (req, res, next) => {
        try {
            const { id } = req.params;
            if (!res.err) res.data = FighterService.deleteFighter(id);
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

module.exports = router;