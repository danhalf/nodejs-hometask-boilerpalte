const { search } = require("../../services/userService");
const { user } = require('../../models/user');
const EMAIL_REGEX =
    /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;
const PHONE_REGEX = /^\+380([5-9][0-9]\d{7})$/;
const AUTHORIZED_DOMAIN = 'gmail.com';
const USER_NAME_VALID_LENGTH = 2;
const USER_PASSWORD_VALID_LENGTH = 2;


function isRequestBodyNotEmpty(requestBody) {
  if (Object.keys(requestBody).length === 0)
    return `request body must be filled`;
  return false
}

function isRequestBodyCorrect(requestBody) {
  for (const key in requestBody) {
    if (key in user) {
      if (key.toLowerCase() === 'id')
        return `body includes 'id' value`
    } else {
      return `request has wrong key: ${ key }`
    }

  }
  return false
}

function isStringLengthValid(strings) {
  for (const key in strings) {
    const value = strings[key];
    let validateLength = 1
    switch (key) {
      case 'firstName':
        validateLength = USER_NAME_VALID_LENGTH
        break
      case 'lastName':
        validateLength = USER_NAME_VALID_LENGTH
        break
      case 'password':
        validateLength = USER_PASSWORD_VALID_LENGTH
        break
    }
    if (!value) return `${ key } empty`;
    if (value.length < validateLength) return `${ key } must be ${ validateLength } or more symbols`;
  }
  return false
}

function isPhoneValid(phoneNumber) {
  if (!phoneNumber) return 'empty phone number field';
  const validPhone = PHONE_REGEX.test(phoneNumber);
  if (!validPhone) return 'Phone number may be correct and starts with +380'
  const isNameAlreadyExists = search({ phoneNumber });
  if (isNameAlreadyExists) return `user with number: -- ${ phoneNumber } -- has already been created`;
  return false
}

function isEmailValid(email) {
  if (!email) return 'empty email field'
  const isNameAlreadyExists = search({ email });
  if (isNameAlreadyExists) return `user with email: -- ${ email } -- has already been created`;
  if (email.length > 254) return 'maximum character value exceeded'
  const validEmail = EMAIL_REGEX.test(email);
  if (!validEmail) return 'email not correct';
  const parts = email.split('@');
  if (parts[0].length > 64 || parts[1] !== AUTHORIZED_DOMAIN)
    return `only ${ AUTHORIZED_DOMAIN } email domain`
  return false
}

module.exports = {
  isRequestBodyNotEmpty,
  isRequestBodyCorrect,
  isStringLengthValid,
  isPhoneValid,
  isEmailValid,
};
