const { fighter } = require('../../models/fighter');
const {searchFighter} = require('../../services/fighterService')

const POWER_MIN_VALUE = 1
const DEFENSE_MIN_VALUE = 1
const POWER_MAX_VALUE = 100
const DEFENSE_MAX_VALUE = 10
const HEALTH_MIN_VALUE = 80
const HEALTH_MAX_VALUE = 120


function isRequestBodyNotEmpty(requestBody) {
  if (Object.keys(requestBody).length === 0)
    return `request body must be filled`;
  return false
}

function isRequestBodyCorrect(requestBody) {
  for (const key in requestBody) {
    if (key in fighter) {
      if (key.toLowerCase() === 'id')
        return `body includes 'id' value`
    } else {
      return `request has wrong key: ${ key }`
    }

  }
  return false
}

function isValidName(name) {
  if(!name) return 'empty fighter name'
  const isNameAlreadyExists = searchFighter({ name });
  if (isNameAlreadyExists) return `fighter ${name} has already been created`;
  return false
}


function isValidValue(fighterValues) {
  for (const key in fighterValues) {
    let minValue, maxValue;
    switch (key) {
      case 'power':
        minValue = POWER_MIN_VALUE;
        maxValue = POWER_MAX_VALUE;
        break
      case 'defense':
        minValue = DEFENSE_MIN_VALUE;
        maxValue = DEFENSE_MAX_VALUE;
        break
      case 'health':
        minValue = HEALTH_MIN_VALUE;
        maxValue = HEALTH_MAX_VALUE;
        break
    }
    const fighterValue = fighterValues[key];
    if (typeof fighterValue !== 'number') return `${ key } value must be a number`;
    const isValidValues = !fighterValue || minValue > fighterValue || fighterValue > maxValue
    if (isValidValues) return `${ key } value can be in the range between ${ minValue } and ${ maxValue }`;
  }
  return false
}


module.exports = {
  isRequestBodyNotEmpty,
  isRequestBodyCorrect,
  isValidName,
  isValidValue,
};
