const {
  isRequestBodyNotEmpty,
  isRequestBodyCorrect,
  isStringLengthValid,
  isPhoneValid,
  isEmailValid,
} = require('./validation/userValidation')

const isUserValid = (req, res, next, put = false) => {
    const request = req.body;

    const validationError = message => {
        if (message !== false) {
            res.status(400).json({ error: true, message })
            next(message)
        }
    }
    if (put) {
        validationError(isRequestBodyNotEmpty({ ...request }))
        validationError(isRequestBodyCorrect({ ...request }))
        validationError(isStringLengthValid({ ...request }))
        'email' in request && validationError(isEmailValid(request.email))
        'phoneNumber' in request && validationError(isPhoneValid(request.phoneNumber))
    } else {
        const { email, password, firstName, lastName, phoneNumber } = request;

        validationError(isRequestBodyNotEmpty(request))
        validationError(isRequestBodyCorrect({...request}))
        validationError(
            isStringLengthValid(
                {
                    firstName, lastName, password
                }))
        validationError(isEmailValid(email))
        validationError(isPhoneValid(phoneNumber))
    }

    next();
}

const createUserValid = (req, res, next) => isUserValid(req, res, next)
const updateUserValid = (req, res, next) => isUserValid(req, res, next, true)


exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
