const { isRequestBodyNotEmpty, isValidValue, isValidName, isRequestBodyCorrect } = require('./validation/fighterValidation')



const checkValid = (req, res, next, put = false) => {
    const request = req.body;

    const validationError = message => {
        if (message !== false) {
            res.status(400).json({ error: true, message })
            next(message)
        }
    }

    if (put) {
        validationError(isRequestBodyNotEmpty({ ...request }))
        validationError(isRequestBodyCorrect({ ...request }))
        validationError(isValidValue({ ...request }))
    } else {
        const { name, power, defense } = request;
        request.health = request.health || 100
        validationError(isRequestBodyCorrect(request))
        validationError(isRequestBodyNotEmpty(request))
        validationError(isValidName(name))
        validationError(isValidValue({power, defense, health: request.health}))
    }


    next();
}

const createFighterValid = (req, res, next) => checkValid(req, res, next)
const updateFighterValid = (req, res, next) => checkValid(req, res, next, true)


exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;