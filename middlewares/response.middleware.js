const responseMiddleware = (req, res, next) => {
    res.data && res.status(200).json(res.data)
    next();
}

exports.responseMiddleware = responseMiddleware;